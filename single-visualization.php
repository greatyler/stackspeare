<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();
			
			the_title();
			
			
			
			echo plugin_dir_path( __FILE__ );
			
			
			?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-1 line-id-badge line-<?php echo $line["stln"]; ?>"><?php echo $line["stln"]; ?></div>
					<div class="div-for-f1-<?php echo $line["stln"]; ?> col-xs-1">
						<button id="f1-button-<?php echo $line["stln"]; ?>" linenum="<?php echo $line["stln"]; ?>" class="f1-button btn line-<?php echo $line["stln"]; ?>">F1</button>
					</div>
					<div class="div-for-q1-<?php echo $line["stln"]; ?> col-xs-1">
						<button id="q1-button-<?php echo $line["stln"]; ?>" linenum="<?php echo $line["stln"]; ?>" class="q1-button btn line-<?php echo $line["stln"]; ?>">Q1</button>
					</div>
					
					<div class="lines-wrapper">
						<div class="hide fline line-<?php echo $line["stln"]; ?> col-xs-1"><?php echo $line["fline"]; ?></div>
						<div class="hide q1line line-<?php echo $line["stln"]; ?> col-xs-1"><?php echo $line["q1line"]; ?></div>
					</div>
				</div>
			</div>
			<?php
			
			
			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php //get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
