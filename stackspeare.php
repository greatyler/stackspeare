<?php
/*
Plugin Name: Stackspeare

Description: Creates a new experience for managing and displaying Shakespeare for WordPress
Version: 0.0.2
Author: Tyler Pruitt
Author URI: http://tpruitt.capeville.wfunet.wfu.edu/tylerpress/
Text Domain: stackspeare
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Bitbucket Plugin URI: greatyler/edudemia-people-tools
Bitbucket Branch: realmaster

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.
Thank You to Robert Vidrine for code contributions, design consultation, and general sanity checks throughout the development process.

The following plugins were sourced or referrenced in this project:
Code from:
edudms



Dependencies or Referrences:





License

College People is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

College Menus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/









include_once('shortcode-stackspeare.php');



include_once('core-types_and_taxes.php');













/********************************************
Scripts and CSS
********************************************/

wp_register_script('prefix_bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js');
wp_enqueue_script('prefix_bootstrap');

wp_register_style('prefix_bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css');
wp_enqueue_style('prefix_bootstrap');

wp_register_script('prefix_jq', 'https://code.jquery.com/jquery-3.3.1.min.js');
wp_enqueue_script('prefix_jq');


wp_register_script('prefix_jqui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js');
wp_enqueue_script('prefix_jqui');

wp_enqueue_script( 'stsp-js', plugins_url() . '/stackspeare/stackspeare.js' );


/********************************************
Custom Single Templates
********************************************/


add_filter( 'single_template', 'stsp_visualization_template' );
function stsp_visualization_template($single_template) {
     global $post;

     if ($post->post_type == 'visualization' ) {
          $single_template = plugin_dir_path( __FILE__ ) . 'single-visualization.php';
     }
     return $single_template;
  
}






















































?>