<?php



// Register Custom Post Type Visualizations
function stsp_register_visualizations() {

	$labels = array(
		'name'                  => _x( 'Visualizations', 'Post Type General Name' ),
		'singular_name'         => _x( 'Visualization', 'Post Type Singular Name' ),
		'menu_name'             => __( 'Visualizations' ),
		'name_admin_bar'        => __( 'Visualization' ),
		'archives'              => __( 'Visualization Archives' ),
		'attributes'            => __( 'Visualization Attributes' ),
		'parent_item_colon'     => __( 'Parent Visualization:' ),
		'all_items'             => __( 'All Visualizations' ),
		'add_new_item'          => __( 'Add New Visualization' ),
		'add_new'               => __( 'Add New' ),
		'new_item'              => __( 'New Visualization' ),
		'edit_item'             => __( 'Edit Visualization' ),
		'update_item'           => __( 'Update Visualization' ),
		'view_item'             => __( 'View Visualization' ),
		'view_items'            => __( 'View Visualizations' ),
		'search_items'          => __( 'Search Visualization' ),
		'not_found'             => __( 'Not found' ),
		'not_found_in_trash'    => __( 'Not found in Trash' ),
		'featured_image'        => __( 'Featured Image' ),
		'set_featured_image'    => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image'    => __( 'Use as featured image' ),
		'insert_into_item'      => __( 'Insert into visualization' ),
		'uploaded_to_this_item' => __( 'Uploaded to this visualization' ),
		'items_list'            => __( 'Visualizations list' ),
		'items_list_navigation' => __( 'Visualizations list navigation' ),
		'filter_items_list'     => __( 'Filter visualizations list' ),
	);
	$args = array(
		'label'                 => __( 'Visualization' ),
		'description'           => __( 'Visualizations of your Play Data' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 30,
		'menu_icon'             => 'dashicons-welcome-widgets-menus',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'visualization', $args );

}
add_action( 'init', 'stsp_register_visualizations', 0 );







// Register Custom Post Type
function stsp_register_lines() {

	$labels = array(
		'name'                  => _x( 'Lines', 'Post Type General Name' ),
		'singular_name'         => _x( 'Line', 'Post Type Singular Name' ),
		'menu_name'             => __( 'Lines' ),
		'name_admin_bar'        => __( 'Lines' ),
		'archives'              => __( 'Line Archives' ),
		'attributes'            => __( 'Line Attributes' ),
		'parent_item_colon'     => __( 'Parent Line:' ),
		'all_items'             => __( 'All Lines' ),
		'add_new_item'          => __( 'Add New Line' ),
		'add_new'               => __( 'Add New' ),
		'new_item'              => __( 'New Line' ),
		'edit_item'             => __( 'Edit Line' ),
		'update_item'           => __( 'Update Line' ),
		'view_item'             => __( 'View Line' ),
		'view_items'            => __( 'View Lines' ),
		'search_items'          => __( 'Search Line' ),
		'not_found'             => __( 'Not found' ),
		'not_found_in_trash'    => __( 'Not found in Trash' ),
		'featured_image'        => __( 'Featured Image' ),
		'set_featured_image'    => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image'    => __( 'Use as featured image' ),
		'insert_into_item'      => __( 'Insert into line' ),
		'uploaded_to_this_item' => __( 'Uploaded to this line' ),
		'items_list'            => __( 'Lines list' ),
		'items_list_navigation' => __( 'Lines list navigation' ),
		'filter_items_list'     => __( 'Filter lines list' ),
	);
	$args = array(
		'label'                 => __( 'Line' ),
		'description'           => __( 'Lines for your plays' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 31,
		'menu_icon'             => 'dashicons-editor-justify',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'line', $args );

}
add_action( 'init', 'stsp_register_lines', 0 );




// Register Custom Post Type Version
function stsp_register_versions() {

	$labels = array(
		'name'                  => _x( 'Versions', 'Post Type General Name' ),
		'singular_name'         => _x( 'Version', 'Post Type Singular Name' ),
		'menu_name'             => __( 'Versions' ),
		'name_admin_bar'        => __( 'Versions' ),
		'archives'              => __( 'Version Archives' ),
		'attributes'            => __( 'Version Attributes' ),
		'parent_item_colon'     => __( 'Parent Version:' ),
		'all_items'             => __( 'All Versions' ),
		'add_new_item'          => __( 'Add New Version' ),
		'add_new'               => __( 'Add New' ),
		'new_item'              => __( 'New Version' ),
		'edit_item'             => __( 'Edit Version' ),
		'update_item'           => __( 'Update Version' ),
		'view_item'             => __( 'View Version' ),
		'view_items'            => __( 'View Versions' ),
		'search_items'          => __( 'Search Version' ),
		'not_found'             => __( 'Not found' ),
		'not_found_in_trash'    => __( 'Not found in Trash' ),
		'featured_image'        => __( 'Featured Image' ),
		'set_featured_image'    => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image'    => __( 'Use as featured image' ),
		'insert_into_item'      => __( 'Insert into version' ),
		'uploaded_to_this_item' => __( 'Uploaded to this version' ),
		'items_list'            => __( 'Versions list' ),
		'items_list_navigation' => __( 'Versions list navigation' ),
		'filter_items_list'     => __( 'Filter versions list' ),
	);
	$args = array(
		'label'                 => __( 'Version' ),
		'description'           => __( 'Versions for your versions' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 33,
		'menu_icon'             => 'dashicons-images-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'version', $args );

}
add_action( 'init', 'stsp_register_versions', 0 );




// Register Custom Post Type Speaker
function stsp_register_speakers() {

	$labels = array(
		'name'                  => _x( 'Speakers', 'Post Type General Name' ),
		'singular_name'         => _x( 'Speaker', 'Post Type Singular Name' ),
		'menu_name'             => __( 'Speakers' ),
		'name_admin_bar'        => __( 'Speakers' ),
		'archives'              => __( 'Speaker Archives' ),
		'attributes'            => __( 'Speaker Attributes' ),
		'parent_item_colon'     => __( 'Parent Speaker:' ),
		'all_items'             => __( 'All Speakers' ),
		'add_new_item'          => __( 'Add New Speaker' ),
		'add_new'               => __( 'Add New' ),
		'new_item'              => __( 'New Speaker' ),
		'edit_item'             => __( 'Edit Speaker' ),
		'update_item'           => __( 'Update Speaker' ),
		'view_item'             => __( 'View Speaker' ),
		'view_items'            => __( 'View Speakers' ),
		'search_items'          => __( 'Search Speaker' ),
		'not_found'             => __( 'Not found' ),
		'not_found_in_trash'    => __( 'Not found in Trash' ),
		'featured_image'        => __( 'Featured Image' ),
		'set_featured_image'    => __( 'Set featured image' ),
		'remove_featured_image' => __( 'Remove featured image' ),
		'use_featured_image'    => __( 'Use as featured image' ),
		'insert_into_item'      => __( 'Insert into speaker' ),
		'uploaded_to_this_item' => __( 'Uploaded to this speaker' ),
		'items_list'            => __( 'Speakers list' ),
		'items_list_navigation' => __( 'Speakers list navigation' ),
		'filter_items_list'     => __( 'Filter speakers list' ),
	);
	$args = array(
		'label'                 => __( 'Speaker' ),
		'description'           => __( 'Speakers for your speakers' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'revisions', 'page-attributes' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 34,
		'menu_icon'             => 'dashicons-universal-access',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'speaker', $args );

}
add_action( 'init', 'stsp_register_speakers', 0 );















































































?>